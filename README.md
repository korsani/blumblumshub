# Name

bbs.pl

# Description

Perl implementation of the Blum-Blum-Shub PRNG algorithm.


# Installation

Ensure those perl modules are installed: ```Math::BigInt::GMP```, ```Math::BigInt::Random```, ```Math::Prime::Util::GMP```

# Usage

	bbs.pl [ --verbose ] [ --outfile <file> ] [ -p <n> -q <n> -s <n> ] [ --strength <n> ] [ --bits <n> ]  [ --statusinterval <n> ] <size>|inf

	bbs.pl --showversions

	bbs.pl --help

# Examples

Generate infinite bytes of random (just to see how "fast" is it):

	bbs.pl -o /dev/null -v inf

Make a 1MB file of random:

	bbs.pl -o /tmp/1M.random -v $((1024*1024))

Show help:

	bbs.pl -h

Try dieharder(3) tests one by one:

	while read t ; do echo ">>> test $t" ; ./bbs.pl -v inf | dieharder -g 200 -d $t ; done <<<$(dieharder -l | awk '{print $2}' | grep -E '[0-9]')

Use pv(1):

	bbs.pl $((1024*1024)) | pv -s $((1024*1024)) > /dev/null

# License

GPL3
