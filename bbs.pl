#!/usr/bin/env perl
# Implémentation de l'algo Blum Blum Shub #useless
# https://crypto.stackexchange.com/questions/72255/blum-blum-shub-pseudo-random-generator-requirements
# http://marcomaggi.github.io/docs/vicare-libs.html/random-generators-bbs.html
# Bench: bbs.pl --str 31 -o /dev/null -v 100000 --bits 1
# RPi 2                                       :  0.6 kbps
# RPi 4                                       :  3.2 kbps
# Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz   :  7.5 kbps
# Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz    : 15.5 kbps
#
# Exemple : bbs.pl --str 42 -o /dev/null -v 5000
use strict;
use warnings;
use bignum;
use Getopt::Long;
use Math::Prime::Util::GMP ':all';
use Math::BigInt::GMP;
use Math::BigInt::Random qw/ random_bigint /;
use Time::HiRes qw( gettimeofday tv_interval setitimer ITIMER_VIRTUAL ITIMER_REAL time);
use Data::Dumper;
use Pod::Usage;
use vars qw( $strengthBits $length $verbose $outfile $p $q $s $m $statusIntervalSeconds $help $version $bits );
use constant PRIME_SAFETY => 5;	# if gcd((p-3)/2,(q-3)/2) is greater than this, consider primes are not safe
my $strengthBits=32;
my $statusIntervalSeconds=2;
my $size=inf;
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub b2gmk {
	my $n=shift;
	my @symbols=('','k','M','G','T');
	for(my $i=1;$i<=scalar(@symbols);$i++) {
		if ($n<=2**(10*$i)) {
			return sprintf "%.1f%s",$n/(2**(10*($i-1))),$symbols[$i-1];
		}
	}
}
sub s2dhms {
    my $s=int(shift);
    my $d=int($s/86400);$s%=86400;
    my $h=int($s/3600);$s%=3600;
    my $min=int($s/60);$s%=60;
    my $dhms;
    $dhms.=$d?$d."d":'';
    $dhms.=$h?$h."h":'';
    $dhms.=$min?$min."min":'';
    $dhms.=$s?$s."s":'';
    return $dhms;
}
sub get_r {
	my $r=Math::BigInt->new(shift);
	while ( ($r % 4) != 3) {
		$r=next_prime($r);
	};
	return $r;
}
sub show_versions {
	foreach my $m ("Math::BigInt", "Math::BigInt::Calc", "Math::BigInt::GMP","Math::BigInt::Random", "Math::Prime::Util::GMP") {
		printf STDERR "%s %s\n",$m,$m->VERSION;
	}
}
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GetOptions(
	'strength=i'	=> \$strengthBits,		# Force : taille, en bits des primes choisis
	'statusinterval=i'	=> \$statusIntervalSeconds,
	'verbose!'		=> \$verbose,
	'showversions!'	=> \$version,
	'outfile=s'		=> \$outfile,		# fichier de sortie
	'q=s'			=> \$q,
	's=s'			=> \$s,
	'p=s'			=> \$p,
	'help'			=> \$help,
	'bits=i'		=> \$bits			# nombre de bits de poids faible à récupérer de la sortie de l'algo
);
if ($version) {
	show_versions;
	exit;
}
if (defined $ARGV[0] and $ARGV[0] =~ /^\d+$/) {
	$size=$ARGV[0];
}
if (not defined $help) {
	$length=Math::BigInt->new($size) if ($size =~ /^\d+$/);
	$length=Math::BigInt->binf() if ($size =~ /inf/i);
} else {
	pod2usage(-verbose => 2);
	exit 0;
}
if ($p and $q) {
	$p=Math::BigInt->new($p);
	$q=Math::BigInt->new($q);
} else {
	$p=Math::BigInt->new(get_r(random_bigint( max => 2**$strengthBits)));
	$q=Math::BigInt->new(get_r(random_bigint( max => 2**$strengthBits)));
	while (	$p->beq($q) or (Math::BigInt->new($p*$q)->bgcd(Math::BigInt->new(($p-1)*($q-1))) != 1 ) or (Math::BigInt->new(($p-3)/2)->bgcd(($q-3)/2) > PRIME_SAFETY)) {
		$q=get_r(next_prime($q));
		if (not defined $q) {			# Si j'y arrive pas, je change de p et q
			$p=get_r(random_bigint( max => 2**$strengthBits));
			$q=get_r(random_bigint( max => 2**$strengthBits));
		}
	}
}
$m=Math::BigInt->new($p*$q);
if ($s) {
	$s=Math::BigInt->new($s);
} else {
	$s=Math::BigInt->new();
	# Choisir une graine telle que gcd(pq,s)=1
	while($s->bgcd($m) != 1) {
		$s=random_bigint(max=>2**$strengthBits);
	}
}
printf STDERR "->p:%s\n->q:%s\n->m:%s\n->safety:%s\n",$p,$q,$m,Math::BigInt->new(($p-3)/2)->bgcd(($q-3)/2) if ($verbose);
if (not defined $bits) {
	$bits=(int(log(log($m))))->numify();
} elsif ($bits > int(log(log($m)))) {
	printf STDERR "WARNING: --bits (%i) greater than log(log(m)) (%.1f). Don't expect generated random to be cryptographicaly sure\n",$bits,log(log($m));
}
printf STDERR "->seed:%s\n->bits:%i\n---<%sbits>---\n",$s,$bits,!$length->is_inf()?b2gmk(8*$length):'∞ ' if ($verbose);
my $x=$s->bmodpow(2,$m);
my $string='';
my $n=0;
# Yala
$|=1;
my $start=time();
if ($outfile) {
	open(STDOUT,">","$outfile") or die "$!";
} elsif ( -t STDOUT) {
	die "STDOUT is your terminal. This is probably a bad idea.\n";
}
my $mask=(1<<$bits)-1;
my $ln=$n;
if ($verbose) {
	$SIG{ALRM}=sub {
		my $d=time()-$start;
		my $eta=!$length->is_inf()?s2dhms($length*$d/$n->numify()-$d):'+∞';
		#                                                                          8*(n-ln)/interval/1024
		printf STDERR "\e[1Gt=%is %sb %.2fkbps eta=%s\e[0K",$d,b2gmk($n->numify()*8),($n-$ln)/$statusIntervalSeconds/128,$eta;
		$ln=$n->numify();
	};
	setitimer(ITIMER_REAL,$statusIntervalSeconds,$statusIntervalSeconds);
}
# Jvais me démerder, pour ça
$SIG{PIPE}='IGNORE';
while($n != $length) {
	# une string de 0 et de 1
	while(length($string)<8) {
		$x=$x*$x % $m;
		# Prends les $bits derniers bits
		$string.=sprintf("%0${bits}b",$x & $mask);
	}
	if (print pack('B8',$string) ) {
		$string='';
		$n+=1;
	} else {
		# Si j'écris sur un pipe et que l'autre bout disparaît, je reçois SIGPIPE et print sort avec EPIPE
		# Mais qd l'autre bout disparaît et que -v, hé bien je reçois aussi EINTR (mais quand pas -v)
		if ($!{EPIPE} or $!{EINTR}) {
			open(STDOUT,">",$outfile);
		} else {
			die "$!";
		}
	}
}
END {
	my $stop=time();
	if (defined $n) {
		my $speed=$n->numify()*8/($stop-$start);
		printf STDERR "\n%sb, %0.2fs, avg=%.2fkbps\n",b2gmk($n->numify()*8),$stop-$start,$speed/1024 if ($verbose);
	}
}
__END__
=pod

=head1 NAME

	bbs.pl - The Blum-Blum-Shub random number generator.

=head1 SYNOPSIS

	bbs.pl [ --verbose ] [ --outfile <file> ] [ -p <n> -q <n> -s <n> ] [ --strength <n> ] [ --bits <n> ]  [ --statusinterval <n> ] <size>|inf

	bbs.pl --showversions

	bbs.pl --help

=head1 DESCRIPTION

Random number generator using BBS (Blum-Blum-Shub) algorithm.

It uses L<Math::BigInt> to go beyond native Perl int size limitations. This Perl implementation is 10^5 times slower than C</dev/urandom>

=head1 OPTIONS

=over

=item <size>

Number of bytes to generate. 'inf' for infinite.

=item --bits

Number of bits to take from each iteration.

The bigger the faster but the weaker.

Default is log(log(m)), m beeing p*q.

=item -p, -q, -s

p, q (larges primes) and s (seed) to use.

They are calculated but you can choose them if you want to test something.

=item --strength

Size of the prime number to use, in bits.

The bigger the stronger but the slower.

Default is B<32>.

=item --statusinterval

Show status every this seconds.

Status shows:

	- elapsed time
	- generated bits
	- speed
	- eventualy the estimated time to completion

Default is B<2>.

=item --outfile

Write generated numbers in this file.

Default is B<stdout>.

=item --verbose

Show choosen p, q, m (p*q) and seed values, prime safety (the lower the better), bits to generate, and status every L<--statusinterval> seconds.

=item --version

Show modules versions.

=back

=head1 EXAMPLES

	# See how fast is you computer (and how slow is this script to generate random)
	bbs.pl -v -o /dev/null inf

	# Generate weak random numbers
	bbs.pl -v -o /dev/null --strength 10

	# Generate 100MB of random
	bbs.pl -o /tmp/100M.random $((100*1024*1024))

	# ... but you are insane
	bbs.pl -o /tmp/100M.random --strength 1024 $((100*1024*1024))

	# ... ... but in hurry
	bbs.pl -o /tmp/100M.random --strength 1024 --bits 6 $((100*1024*1024))

	# Generate an RNG pipe
	mkfifo /tmp/bbs.fifo
	bbs.pl -o /tmp/bbs.fifo inf &

	# Test randomness with dieharder(3)
	bbs.pl -v inf | dieharder -g 200 -a

	# Use pv(1) (it prevents you from Math::BigInt "Argument isn't numeric" bug)
	bbs.pl inf | pv > /dev/null
	bbs.pl $((10*1024*1024)) | pv -s $((10*1024*1024)) > /dev/null

=head1 BUGS

Slow.

Sometimes this message appears:

	Argument "1\01^EU\0\0" isn't numeric in numeric comparison (<=>) at /usr/share/perl5/Math/BigInt/Calc.pm line 971.

It's harmless.

Known to fail:

- L<Math::BigInt> 1.999830 (C<numify()> problem), 1.999832 ("Deep recursion on subroutine "Math::BigInt::bmodinv""), 1.999832 (Same), 1.999833 (Same), 1.999834 ("Not a CODE reference"), 1.999835 (Same), 1.999836 (Same), 1.999837 (Same)

Known to be good:

- L<Math::BigInt> 1.999816, 1.999818, 1.999829 (Despite "Argument is not numeric" problem)

- L<Math::BigInt::Random> 0.04

- L<Math::BigInt::GMP> 1.6006, 1.6007, 1.6010

=head1 SEE ALSO

L<https://fr.wikipedia.org/wiki/Blum_Blum_Shub>

dieharder(3)
